import React from 'react';
import { Container } from 'react-bootstrap';
import MyTable from './components/table/MyTable';
import Header from './components/header/header';
import NavMenu from './components/navs/navmenu';
import moment from "moment";

import { Switch, Route, BrowserRouter } from 'react-router-dom';
import './App.css';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{
        "name": "PUBG Mobile",
        "region": "US",
        "createdOn": 1559807714999,
        "price": "Price info of Test Whatsapp",
        "csv": "Some CSV link for Whatsapp",
        "report": "Some report link for Whatsapp",
        "image_url": "/images/Bitmap (5).png",
        "status":"upcoming"
      },
      {
        "name": "Auto Chess",
        "region": "CA, FR",
        "createdOn": 1576763332000,
        "price": "Price info of Super Jewels Quest",
        "csv": "Some CSV link for Super Jewels Quest",
        "report": "Some report link for Super Jewels Ques",
        "image_url": "/images/Bitmap.png",
        "status":"upcoming"
      },
      {
        "name": "Flow Free",
        "region": "FR",
        "createdOn": 1559806711124,
        "price": "Price info of Mole Slayer",
        "csv": "Some CSV link for Mole Slayer",
        "report": "Some report link for Mole Slayer",
        "image_url": "/images/Bitmap (2).png",
        "status":"upcoming"
      },
      {
        "name": "Gareena Free Fire",
        "region": "JP",
        "createdOn": 1559806680124,
        "price": "Price info of Mancala Mix",
        "csv": "Some CSV link for Mancala Mix",
        "report": "Some report link for Mancala Mix",
        "image_url": "/images/Bitmap (3).png",
        "status":"upcoming"
      },
      {
        "name": "Mortal Kombat",
        "region": "US",
        "createdOn": 1559807714999,
        "price": "Price info of Test Whatsapp",
        "csv": "Some CSV link for Whatsapp",
        "report": "Some report link for Whatsapp",
        "image_url": "/images/Bitmap (4).png",
        "status":"upcoming"
      },
      {
        "name": "Summoners War",
        "region": "CA, FR",
        "createdOn": 1559806715124,
        "price": "Price info of Super Jewels Quest",
        "csv": "Some CSV link for Super Jewels Quest",
        "report": "Some report link for Super Jewels Ques",
        "image_url": "/images/Bitmap (8).png",
        "status":"upcoming"
      },
      {
        "name": "Need for Speed No Limits",
        "region": "FR",
        "createdOn": 1559806711124,
        "price": "Price info of Mole Slayer",
        "csv": "Some CSV link for Mole Slayer",
        "report": "Some report link for Mole Slayer",
        "image_url": "/images/Bitmap (6).png",
        "status":"upcoming"
      },
      {
        "name": "Shadow Fight 3",
        "region": "JP",
        "createdOn": 1559806680124,
        "price": "Price info of Mancala Mix",
        "csv": "Some CSV link for Mancala Mix",
        "report": "Some report link for Mancala Mix",
        "image_url": "/images/Bitmap (7).png",
        "status":"upcoming"
      },
      {
        "name": "PUBG Mobile",
        "region": "US",
        "createdOn": 1559807714999,
        "price": "Price info of Test Whatsapp",
        "csv": "Some CSV link for Whatsapp",
        "report": "Some report link for Whatsapp",
        "image_url": "/images/Bitmap (5).png",
        "status":"live"
      },
      {
        "name": "Auto Chess",
        "region": "CA, FR",
        "createdOn": 1559806715124,
        "price": "Price info of Super Jewels Quest",
        "csv": "Some CSV link for Super Jewels Quest",
        "report": "Some report link for Super Jewels Ques",
        "image_url": "/images/Bitmap.png",
        "status":"live"
      },
      {
        "name": "Flow Free",
        "region": "FR",
        "createdOn": 1559806711124,
        "price": "Price info of Mole Slayer",
        "csv": "Some CSV link for Mole Slayer",
        "report": "Some report link for Mole Slayer",
        "image_url": "/images/Bitmap (2).png",
        "status":"live"
      },
      {
        "name": "Gareena Free Fire",
        "region": "JP",
        "createdOn": 1559806680124,
        "price": "Price info of Mancala Mix",
        "csv": "Some CSV link for Mancala Mix",
        "report": "Some report link for Mancala Mix",
        "image_url": "/images/Bitmap (3).png",
        "status":"live"
      },
      {
        "name": "Mortal Kombat",
        "region": "US",
        "createdOn": 1559807714999,
        "price": "Price info of Test Whatsapp",
        "csv": "Some CSV link for Whatsapp",
        "report": "Some report link for Whatsapp",
        "image_url": "/images/Bitmap (4).png",
        "status":"live"
      },
      {
        "name": "Summoners War",
        "region": "CA, FR",
        "createdOn": 1559806715124,
        "price": "Price info of Super Jewels Quest",
        "csv": "Some CSV link for Super Jewels Quest",
        "report": "Some report link for Super Jewels Ques",
        "image_url": "/images/Bitmap (8).png",
        "status":"live"
      },
      {
        "name": "Need for Speed No Limits",
        "region": "FR",
        "createdOn": 1559806711124,
        "price": "Price info of Mole Slayer",
        "csv": "Some CSV link for Mole Slayer",
        "report": "Some report link for Mole Slayer",
        "image_url": "/images/Bitmap (6).png",
        "status":"live"
      },
      {
        "name": "Shadow Fight 3",
        "region": "JP",
        "createdOn": 1559806680124,
        "price": "Price info of Mancala Mix",
        "csv": "Some CSV link for Mancala Mix",
        "report": "Some report link for Mancala Mix",
        "image_url": "/images/Bitmap (7).png",
        "status":"live"
      },
      {
        "name": "PUBG Mobile",
        "region": "US",
        "createdOn": 1559807714999,
        "price": "Price info of Test Whatsapp",
        "csv": "Some CSV link for Whatsapp",
        "report": "Some report link for Whatsapp",
        "image_url": "/images/Bitmap (5).png",
        "status":"past"
      },
      {
        "name": "Auto Chess",
        "region": "CA, FR",
        "createdOn": 1559806715124,
        "price": "Price info of Super Jewels Quest",
        "csv": "Some CSV link for Super Jewels Quest",
        "report": "Some report link for Super Jewels Ques",
        "image_url": "/images/Bitmap.png",
        "status":"past"
      },
      {
        "name": "Flow Free",
        "region": "FR",
        "createdOn": 1559806711124,
        "price": "Price info of Mole Slayer",
        "csv": "Some CSV link for Mole Slayer",
        "report": "Some report link for Mole Slayer",
        "image_url": "/images/Bitmap (2).png",
        "status":"past"
      },
      {
        "name": "Gareena Free Fire",
        "region": "JP",
        "createdOn": 1559806680124,
        "price": "Price info of Mancala Mix",
        "csv": "Some CSV link for Mancala Mix",
        "report": "Some report link for Mancala Mix",
        "image_url": "/images/Bitmap (3).png",
        "status":"past"
      },
      {
        "name": "Mortal Kombat",
        "region": "US",
        "createdOn": 1559807714999,
        "price": "Price info of Test Whatsapp",
        "csv": "Some CSV link for Whatsapp",
        "report": "Some report link for Whatsapp",
        "image_url": "/images/Bitmap (4).png",
        "status":"past"
      },
      {
        "name": "Summoners War",
        "region": "CA, FR",
        "createdOn": 1559806715124,
        "price": "Price info of Super Jewels Quest",
        "csv": "Some CSV link for Super Jewels Quest",
        "report": "Some report link for Super Jewels Ques",
        "image_url": "/images/Bitmap (8).png",
        "status":"past"
      },
      {
        "name": "Need for Speed No Limits",
        "region": "FR",
        "createdOn": 1559806711124,
        "price": "Price info of Mole Slayer",
        "csv": "Some CSV link for Mole Slayer",
        "report": "Some report link for Mole Slayer",
        "image_url": "/images/Bitmap (6).png",
        "status":"past"
      },
      {
        "name": "Shadow Fight 3",
        "region": "JP",
        "createdOn": 1559806680124,
        "price": "Price info of Mancala Mix",
        "csv": "Some CSV link for Mancala Mix",
        "report": "Some report link for Mancala Mix",
        "image_url": "/images/Bitmap (7).png",
        "status":"past"
      }
      ]};
      this.routes = [
        {
          id: 'upcoming',
          isActive: true,
          path: '/',
          RouteComponent: MyTable,
          type:'upcoming'
          //data: this.state.data.filter((elem)=>elem.status === 'upcoming')
        },
        {
          id: 'live',
          isActive: true,
          path: '/live',
          RouteComponent: MyTable,
          type:'live'
          //data: this.state.data.filter((elem)=>elem.status === 'live')
        },
        {
          id: 'past',
          isActive: true,
          path: '/past',
          RouteComponent: MyTable,
          type:'past'
          //data: this.state.data.filter((elem)=>elem.status === 'past')
        },
      ];
      this.updateDateInState=this.updateDateInState.bind(this);
      }
  
  componentDidMount() {
    //make api request to fetch data
  }

checkAppropriateStatus(date){
  let days = moment().diff(date, 'days') // -8 (days)
    if (days < 0)//future date
    {
      return 'upcoming';
    }
    else if (days===0){
      return 'live';
    } else return 'past'
}

updateDateInState(date,elem){
 let data = [...this.state.data];
 let newElem = data.findIndex((obj)=>obj.status===elem.status&&obj.createdOn===elem.createdOn&&obj.name===elem.name) 
 data.splice(newElem,1);
 let elm = {...elem};
 elm.createdOn = date.getTime();
 let status = this.checkAppropriateStatus(date);
 elm.status=status;
 data.push(elm);
 this.setState({data});

}

  render() {

    return (
      <div className="content">
        <Header />
        <Container>
          <BrowserRouter>
            <NavMenu updateLanguage={this.props.updateLanguage} locale ={this.props.locale}></NavMenu>
            <Switch>
            {this.routes.map(({ id, path, RouteComponent, type }) => (
                  <Route
                    key={`route-${id}`}
                    path={path}
                    exact
                    render={routeProps => <RouteComponent {...routeProps} data={this.state.data.filter((elem)=>elem.status === type)} updateDateInState={this.updateDateInState} />}
                  />
                ))}
            </Switch>
          </BrowserRouter>
        </Container>
      </div>
    );
  }

}
export default App;
