import React from 'react';
import { NavLink } from 'react-router-dom';
import './navmenu.css';
import { FormattedMessage } from 'react-intl';
import {Dropdown} from 'react-bootstrap';




export default (props) =>{ 
    function updateLanguage(locale){
        if(props.locale!==locale){
            props.updateLanguage(locale);
        }
      }
    return (<header>
        <h1 className="title">
            <FormattedMessage id="site-title"
                defaultMessage="Manage Campaigns"
            />
        </h1>
        <div className="languageDropdown">
            <Dropdown>
                <Dropdown.Toggle variant="info" id="dropdown-basic" >
                    Language Selection
  </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item eventKey="en" onClick={()=>updateLanguage('en')}>English</Dropdown.Item>
                    <Dropdown.Item eventKey="de" onClick={()=>updateLanguage('de')}>German</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
          
        </div>
        <nav>
            <ul className="navMenu">
                <li><NavLink exact to='/'><FormattedMessage id="upcoming"
                    defaultMessage="Upcoming Campaigns"
                /></NavLink></li>
                <li><NavLink to='/live'>
                    <FormattedMessage id="live"
                        defaultMessage="Live Campaigns"
                    />
                </NavLink></li>
                <li><NavLink to='/past'>
                    <FormattedMessage id="past"
                        defaultMessage="Past Campaigns"
                    />
                </NavLink></li>
            </ul>

        </nav>
    </header>
)};