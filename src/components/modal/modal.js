import React from 'react';
import './modal.css';
const Modal = ({ handleClose, show, game }) => {
    const showHideClassName = show ? "mymodal display-block" : "mymodal display-none";
    let { data } = game;
    return (
        <div className={showHideClassName}>
            <section className="modal-main">
                <div className="head">
                    <div>
                    <img src={data && data.image_url ? data.image_url : ""} alt="data logo"></img>
                    </div>
                    <div className="titleText">
                        <div className="bold">{data && data.name ? data.name : ""}</div>
                        <div className="smallText">{data && data.region ? data.region : ""}</div>
                    </div>
                </div>
                <div className="modal-body">
                    <h4>Pricing</h4>
                    <div className="floatContent">
                        <div className="left">1 Week - 1 Month</div>
                        <div className="right bold">$ 100.00</div>
                    </div>
                    <div className="floatContent">
                        <div className="left">6 Months</div>
                        <div className="right bold">$ 500.00</div>
                    </div>
                    <div className="floatContent">
                        <div className="left">1 Year</div>
                        <div className="right bold">$ 900.00</div>
                    </div>
                </div>
                <div className="center">
                    <button onClick={handleClose}>Close</button>
                </div>
            </section>
        </div>
    );
};

export default Modal; 