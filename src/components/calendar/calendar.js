
import React, { Component } from 'react';
import DatePicker from 'react-date-picker';
import './calendar.css' 
class Calendar extends Component {
  state = {
    date: new Date(),
  }
 
  onChange = date => {
      this.setState({ date })
      this.props.triggerUpdateDate(date);
    }
 
  render() {
    const showHideClassName = this.props.isOpen ? "display-block" : "display-none";
    return (
      <div className={showHideClassName}>
        <DatePicker
          onChange={this.onChange}
          value={this.state.date}
        />
      </div>
    );
  }
}

export default Calendar