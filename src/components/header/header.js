import React from 'react';
import './header.css';

export default () => (
<header className="header">
         <div className="imgDiv">
             <img src="https://cdn-www.bluestacks.com/bs-images/new-logo-white.png" alt="site logo"></img>
         </div>
</header>
);

