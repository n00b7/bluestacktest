import React, { Component } from 'react';

import { IntlProvider as ReactIntlProvider } from 'react-intl';
import { de } from '../../translations/de';
import { en } from '../../translations/en';
import App from '../../App';

class IntlProvider extends Component {
 
    constructor(props){
        super(props);
        this.state = {
            locale:'en',
            messages:en
        }
    this.updateLanguage = this.updateLanguage.bind(this);
      }

 
  updateLanguage(locale){
      if(this.state.locale !== locale){
          this.setState({locale,messages:locale==='en'?en:de})
      }
  }

  

  render() {
    const {  locale, messages } = this.state;

    return (
      messages && (
        <ReactIntlProvider
          key={locale}
          locale={locale}
          messages={messages}
          textComponent={props => props.children}
        >
          <App updateLanguage={this.updateLanguage} locale ={this.state.locale}></App>
        </ReactIntlProvider>
      )
    );
  }
}

export default IntlProvider;
