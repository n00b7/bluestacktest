import React, { useState } from "react";
import './MyTable.css';
import Modal from '../modal/modal';
import moment from "moment";
import Cal from '../calendar/calendar';


function MyTable(props) {
  let i = 0;
  const [isModal, setState] = useState(false);
  const [modalData, setModalState] = useState({});
  const [calIsOpen, toggleCal] = useState(false);

  function closeModal() {
    if (isModal) {
      setState(false);
    }
  }

  function showModal(data) {
    if (!isModal) {
      setState(true);
      setModalState({ data });
    }
  }

  function updateDate(date, elem) {
    toggleCal(false);
    props.updateDateInState(date, elem);
  }

  function calculateDaysFromToday(date) {
    let days = moment().diff(date, 'days') // -8 (days)
    if (days < 0)//future date
    {
      return `${parseInt(days * -1)} Days Ahead`;
    }
    else {
      return `${parseInt(days)} Days Ago`;
    }
  }

  return (
    <div>
      <table className="table-grid" role="table">
        <thead>
          <tr role="row">
            <th role="columnheader">Date</th>
            <th role="columnheader">Campaign</th>
            <th role="columnheader">View</th>
            <th role="columnheader">Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.data.map(elem => (
            <tr role="row" className="my-3  " key={i++}>
              <td role="cell" className="text-bold">
                <div className="bold">{moment(elem.createdOn).format("MMM YYYY, DD")}</div>
                <div className="smallText">{calculateDaysFromToday(elem.createdOn)} </div>
              </td>
              <td role="cell">
                <div className="row ">
                  <div className="col-sm-12 col-md-2 col-lg-2">
                    <img
                      className="img-responsive avator"
                      src={elem.image_url}
                      alt="game logo"
                    ></img>
                  </div>
                  <div className="col-sm-12 col-md-10 col-lg-10 text-md-left">
                    <div className="bold">{elem.name}</div>
                    <div className="text-italic">{elem.region}</div>
                  </div>
                </div>
              </td>
              <td role="cell" onClick={() => showModal(elem)}>
                <img src="images/price.png" alt="test" />
                Check Price
                </td>
              <td role="cell">
                <div className="actions">
                  <div>
                    <img src="images/file.png" alt="test" /> CSV
                    </div>
                  <div>
                    <img src="images/statistics-report.png" alt="test" /> Report
                    </div>
                  <div> 
                    <img src="images/calendar.png" alt="test" onClick={()=>toggleCal(true)}/>
                    Schedule Again
                    <div className="calendarPopover"><Cal isOpen={calIsOpen} triggerUpdateDate={(date)=>updateDate(date,elem)}/></div>
                    
                    </div>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Modal handleClose={closeModal} show={isModal} game={modalData}></Modal>
    </div>
  );
}

export default MyTable;
